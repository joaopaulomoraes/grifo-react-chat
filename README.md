# Grifo React Chat

FrontEnd test for gri.fo chat

## Getting Started

### Installing

Cloning the project.
```
git clone https://joaopaulomoraes@bitbucket.org/joaopaulomoraes/grifo-react-chat.git chat-react-test
```

Access the project directory.
```
cd chat-react-test
```

Install dependencies.
```
yarn install
```

Serve with hot reload at http://localhost:3000.
```
yarn start
```


## License

This project is licensed under the MIT License.
