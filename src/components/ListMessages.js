import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'reactstrap'

const ListMessages = ({ messages }) => {
  return (
    <div className="space-between-messages">
      {messages &&
        messages.map((message, i) => (
          <Row key={i} className="list-messages" style={{ flexDirection: message.type === 'received' ? 'row' : 'column' }}>
            <Col xs="3" sm="2" md="2" lg="1">
              {message.thumbnail && (
                <img src={message.thumbnail} alt="Avatar" className="avatar" />
              )}
            </Col>
            <Col
              xs="auto"
              className={
                message.type === 'received'
                  ? 'message-received'
                  : 'message-sent'
              }
            >
              {message.textMessage}
            </Col>
          </Row>
        ))}
    </div>
  )
}

ListMessages.defaultProps = {
  messages: []
}

ListMessages.propTypes = {
  messages: PropTypes.array.isRequired
}

export default ListMessages
