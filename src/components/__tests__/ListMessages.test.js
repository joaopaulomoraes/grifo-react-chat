import React from 'react'
import { render } from 'enzyme'
import ListMessages from '../../components/ListMessages'

describe('[component] ListMessages', () => {
  it('renderiza o componente dado uma lista de mensagens', () => {
    expect(render(<ListMessages messages={[]} />))
  })
})

export default ListMessages
