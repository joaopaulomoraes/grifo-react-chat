import React from 'react'
import { shallow } from 'enzyme'
import SendMessages from '../../components/SendMessages'


describe('[component] SendMessages', () => {
  const defaultValue = 'current message'
  const setMessage = jest.fn()
  const sendMessage = jest.fn()

  it('verifica se o metodo onChange recebe um determinado valor', () => {
    const wrapper = shallow(
      <SendMessages
        defaultValue={defaultValue}
        setMessage={setMessage}
        sendMessage={sendMessage}
      />
    )

    wrapper.find('input').simulate('change', {
      target: { value: defaultValue }
    })
    expect(setMessage).toBeCalledWith(defaultValue)
  })

  it('verifica se o metodo sendMessage está sendo chamado quando o botão é clicado', () => {  
    const wrapper = shallow(
      <SendMessages
        defaultValue={defaultValue}
        setMessage={setMessage}
        sendMessage={sendMessage}
      />
    )

    wrapper.find('button').simulate('click')
    expect(sendMessage).toHaveBeenCalledTimes(1)
  })
})
