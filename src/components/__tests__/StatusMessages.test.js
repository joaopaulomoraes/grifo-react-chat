import React from 'react'
import { shallow, render } from 'enzyme'
import StatusMessage from '../../components/StatusMessages'

const props = {
  receivedMessages: 0,
  sentMessages: 0
}

describe('[component] StatusMessages', () => {
  it('renderiza o número de mensagens enviadas e recebidas dados seus valores', () => {
    expect(shallow(
      <StatusMessage {...props} />
    ))
  })

  it('falha se o número de mensagens enviadas e recebidas não for passada por props', () => {
    const wrapper = render(
      <StatusMessage />
    )
    expect(wrapper.text()).toContain('0 Online Requests 0 Offline Requests')
  })
})
