import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'reactstrap'

const SendMessages = ({ defaultValue, setMessage, sendMessage }) => {
  return (
    <Row style={{ marginTop: '4vw' }}>
      <Col
        xs="10"
        sm="11"
      >
        <input
          autoFocus
          type="text"
          className="send-messages-input"
          defaultValue={defaultValue}
          onChange={e => setMessage(e.target.value)}
        />
      </Col>
      <Col
        xs="2"
        sm="1"
        className="flex-btn"
        style={{ 
          display: 'flex',
          flexDirection: 'column',
          paddingRight: 0
        }}
      >
        <button
          className="btn"
          onClick={() => sendMessage(defaultValue)}
        >
          <i className="fa fa-paper-plane" />
        </button>
      </Col>
    </Row>
  )
}

SendMessages.propTypes = {
  defaultValue: PropTypes.string.isRequired,
  setMessage: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired
}

export default SendMessages
