import React, { Component } from 'react'
import ChatAppContainer from '../containers/ChatAppContainer'

class App extends Component {
  render() {
    return (
      <ChatAppContainer />
    )
  }
}

export default App
