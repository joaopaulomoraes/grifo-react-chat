import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'reactstrap'

const StatusMessages = ({ receivedMessages, sentMessages }) => {
  return (
    <Col sm={{ size: 8, offset: 2 }}>
      <Row className="status-messages">
        <Col style={{ textAlign: 'right' }}>
          <i className="fa fa-comments" /> {receivedMessages} Online Requests
        </Col>
        <Col style={{ color: '#CACACA' }}>
          <i className="fa fa-envelope" /> {sentMessages} Offline Requests
        </Col>
      </Row>
    </Col>
  )
}

StatusMessages.defaultProps = {
  receivedMessages: 0,
  sentMessages: 0
}

StatusMessages.propTypes = {
  receivedMessages: PropTypes.number.isRequired,
  sentMessages: PropTypes.number.isRequired
}

export default StatusMessages
