import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { messages } from '../../api'
import {
  getMessages,
  sendMessage,
  GET_MESSAGES,
  SEND_MESSAGE
} from '../../actions'

const mockStore = configureMockStore([ thunk ])
const store = mockStore({
  messages
})

describe('action creators', () => {
  afterEach(() => store.clearActions())

  it('GET_MESSAGES', () => {
    const expectedActions = [{ type: GET_MESSAGES, messages }]

    return store.dispatch(getMessages())
    .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  const message = {
    textMessage: 'Send new message action',
    type: 'sent'
  }
  it('SEND_MESSAGE', () => {
    const expectedActions = [{ type: SEND_MESSAGE, message }]

    return store.dispatch(sendMessage(message))
    .then(() => expect(store.getActions()).toEqual(expectedActions))
  })
})
