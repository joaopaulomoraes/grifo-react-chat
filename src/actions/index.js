export const SEND_MESSAGE = 'SEND_MESSAGE'
export const GET_MESSAGES = 'GET_MESSAGES'

/* eslint-disable */ import * as chatAPI from '../api'

/**
 * @description Solicita todas as mensagens
 * @returns {object} action - O tipo da action e um novo objeto com as mensagens
 */
export const getMessages = () => {
  return dispatch => chatAPI.getMessages().then(messages =>
    dispatch({ type: GET_MESSAGES, messages })
  )
}

/**
 * @description Envia uma mensagem
 * @param {object} message - Um novo objeto com o texto da mensagem e seu tipo
 * @returns {object} action - O tipo da action e um novo objeto com a mensagem específica
 */
export const sendMessage = message => {
  return dispatch => chatAPI.sendMessage(message).then(message =>
    dispatch({ type: SEND_MESSAGE, message })
  )
}
