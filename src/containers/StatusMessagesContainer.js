import React from 'react'
import PropTypes from 'prop-types'
import StatusMessages from '../components/StatusMessages'

const StatusMessagesContainer = ({ messages }) => {
  /**
   * @description Filtra todos as mensagens com o tipo received
   * @returns {Object} Um novo objeto apenas com as mesagens filtradas
   */
  const receivedMessages = () => {
    return messages && messages.filter(message => message.type === 'received')
  }

  /**
   * @description Filtra todos as mensagens com o tipo sent
   * @returns {Object} Um novo objeto apenas com as mesagens filtradas
   */
  const sentMessages = () => {
    return messages && messages.filter(message => message.type === 'sent')
  }

  return (
    <StatusMessages
      receivedMessages={receivedMessages() && receivedMessages().length}
      sentMessages={sentMessages() && sentMessages().length}
    />
  )
}

StatusMessagesContainer.defaultProps = {
  messages: []
}

StatusMessagesContainer.propTypes = {
  messages: PropTypes.array.isRequired
}

export default StatusMessagesContainer
