import React, { Component } from 'react'
import { connect } from 'react-redux'
import { sendMessage } from '../actions'
import SendMessages from '../components/SendMessages'

class SendMessagesContainer extends Component {
  state = {
    message: '',
    countMessages: 0
  }

  /**
   * @description Define a mensagem recebida do input no estado atual do componente
   * @param {string} message - A mesagem a ser setada no estado
   */
  setMessage = message => {
    message && this.setState({ message })
  }

  /**
   * @description Conta as mesagens para definir o tipo específico
   */
  countMessages = () => {
    this.setState((prevState) => ({
      countMessages: prevState.countMessages + 1
    }))
  }

  /**
   * @description Define o tipo da mensagem a ser enviada com base na contagem do estado
   * @returns {string} - O tipo a ser setado no envio (received | sent)
   */
  typeMessage = () => {
    return this.state.countMessages % 2 === 0 ? 'received' : 'sent'
  }

  /**
   * @description Dispacha um objeto no Redux para atualização do estado
   * @param {string} message - Mesagem a ser definida no objeto
   */
  sendMessage = message => {
    message && this.props.sendMessage({ 
      textMessage: message,
      type: this.typeMessage(),
      thumbnail: this.typeMessage() === 'received' ? 'http://www.coiffeur-venette.com/coupe-femme-coiffeuse-venette.jpg' : null
    })
    
    this.countMessages()
  }

  render() {
    return (
      <SendMessages
        defaultValue={this.state.message}
        setMessage={this.setMessage}
        sendMessage={this.sendMessage}
      />
    )
  }
}

const mapDispatchToProps = dispatch => ({
  sendMessage: message => dispatch(sendMessage(message))
})

export default connect(
  null,
  mapDispatchToProps
)(SendMessagesContainer)
