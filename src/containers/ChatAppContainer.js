import React, { Component } from 'react'
import StatusMessagesContainer from '../containers/StatusMessagesContainer'
import ListMessages from '../components/ListMessages'
import SendMessagesContainer from '../containers/SendMessagesContainer'
import { connect } from 'react-redux'
import { getMessages } from '../actions'

class ChatAppContainer extends Component {
  /**
   * @description Simula o carregamento de todas as mensagens do servidor
   */
  componentDidMount() {
    this.props.getAllMessages()
  }

  render() {
    const { messages } = this.props
    return (
      <div>
        <StatusMessagesContainer messages={messages} />
        <ListMessages messages={messages} />
        <SendMessagesContainer />
      </div>
    )
  }
}

const mapStateToProps = ({ messages }) => {
  return {
    messages
  }
}

const mapDispatchToProps = dispatch => ({
  getAllMessages: () => dispatch(getMessages())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatAppContainer)
