/**
 * @description Lista de mensagens imaginária
 */
export const messages = [
  {
    textMessage: 'Hi, there!',
    type: 'received',
    thumbnail: 'http://www.coiffeur-venette.com/coupe-femme-coiffeuse-venette.jpg'
  },
  {
    textMessage: 'Hi! How are you!',
    type: 'sent'
  }
]

/**
 * @description Promise para receber todas as mensagens do servidor imaginário
 * @returns {Promise} Uma Promise que representa uma lista de mensagens
 */
export const getMessages = async () => {
  return await messages
}

/**
 * @description Promise para enviar uma mensagen específica ao servidor imaginário
 * @param {object} message - Texto da mensagem e tipo
 * @returns {Promise} Uma Promise que representa uma nova mensagem
 */
export const sendMessage = async message => {
  return await message
}
