import messages from '../'
import { messages as currentState } from '../../api'
import { GET_MESSAGES, SEND_MESSAGE } from '../../actions'

describe('messages reducer', () => {
  it('deve lidar com o estado inicial', () => {
    expect(messages(undefined, {})).toEqual({})
  })

  it('deve suportar uma ação com o tipo GET_MESSAGES', () => {
    expect(messages([], {
      type: GET_MESSAGES,
      messages: currentState
    }))
    .toEqual({ messages: currentState })
  })

  const message = {
    textMesasge: 'New message',
    type: 'sent'
  }
  it('deve suportar uma ação com o tipo SEND_MESSAGE', () => {
    expect(messages([message], {
      type: SEND_MESSAGE,
      message
    }))
    .toEqual({ 0: message })
  })
})