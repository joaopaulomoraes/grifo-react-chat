import {
  GET_MESSAGES,
  SEND_MESSAGE
} from '../actions'

const messages = (state = {}, action) => {
  switch (action.type) {
    case GET_MESSAGES:
      return {
        ...state,
        messages: action.messages
      }

    case SEND_MESSAGE:
      return {
        ...state,
        messages: state.messages && state.messages.concat([action.message])
      }

    default:
      return state
  }
}

export default messages
